<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Institucional;
use App\Models\CdLogistico;
use App\Models\Galeria;

class EmpresaController extends Controller
{
    public function institucional()
    {
        $institucional = Institucional::first();

        return view('frontend.empresa.institucional', compact('institucional'));
    }

    public function cdLogistico()
    {
        $cdLogistico = CdLogistico::first();

        return view('frontend.empresa.cd-logistico', compact('cdLogistico'));
    }

    public function galeria()
    {
        $galerias = Galeria::ordenados()->paginate(6);

        return view('frontend.empresa.galeria.index', compact('galerias'));
    }

    public function galeriaShow(Galeria $galeria)
    {
        return view('frontend.empresa.galeria.show', compact('galeria'));
    }
}
