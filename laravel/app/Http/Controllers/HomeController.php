<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\NewsletterRequest;
use App\Models\Newsletter;

use App\Models\Banner;
use App\Models\Chamada;
use App\Models\Home;
use App\Models\Galeria;
use App\Models\Depoimento;

use App\Models\Institucional;
use App\Models\CdLogistico;

use App\Helpers\Tools;
use App\Models\ProdutoCategoria;
use App\Models\Produto;

class HomeController extends Controller
{
    public function index()
    {
        $banners     = Banner::ordenados()->get();
        $chamadas    = Chamada::ordenados()->get();
        $home        = Home::first();
        $novidades   = Galeria::ordenados()->take(3)->get();
        $depoimentos = Depoimento::ordenados()->get();

        return view('frontend.home', compact('banners', 'chamadas', 'home', 'novidades', 'depoimentos'));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => t('footer.success')
        ]);
    }

    public function busca()
    {
        $termo = request('termo');

        if (! $termo) return redirect()->route('home');

        $resultados = [];

        if (Institucional::busca($termo)->count()) {
            $resultados[] = [
                'area'   => t('nav.america-uv'),
                'titulo' => t('nav.institucional'),
                'link'   => route('empresa.institucional'),
                'texto'  => Tools::textoBusca(tobj(Institucional::first(), 'texto'))
            ];
        }

        if (CdLogistico::busca($termo)->count()) {
            $resultados[] = [
                'area'   => t('nav.america-uv'),
                'titulo' => t('nav.cd-logistico'),
                'link'   => route('empresa.cd-logistico'),
                'texto'  => Tools::textoBusca(tobj(CdLogistico::first(), 'texto_1'))
            ];
        }

        foreach(Galeria::busca($termo)->ordenados()->get() as $g) {
            $resultados[] = [
                'area'   => t('nav.galeria'),
                'titulo' => tobj($g, 'titulo'),
                'link'   => route('empresa.galeria.show', $g->slug),
                'texto'  => Tools::textoBusca(tobj($g, 'texto'))
            ];
        }

        foreach(ProdutoCategoria::busca($termo)->ordenados()->get() as $c) {
            $resultados[] = [
                'area'   => t('nav.produtos'),
                'titulo' => t('nav.'.$c->slug),
                'link'   => in_array($c->slug, ['materiais', 'tinta-digital-uv-led-hnd-ink']) ? route($c->slug) : route('tecnologias', $c->slug),
                'texto'  => Tools::textoBusca(tobj($c, 'texto'))
            ];
        }

        foreach(Produto::busca($termo)->ordenados()->get() as $p) {
            $resultados[] = [
                'area'   => t('nav.'.$p->categoria->slug),
                'titulo' => tobj($p, 'titulo'),
                'link'   => in_array($p->categoria->slug, ['materiais', 'tinta-digital-uv-led-hnd-ink']) ? route($p->categoria->slug.'.show', $p->slug) : route('tecnologias.show', [$p->categoria->slug, $p->slug]),
                'texto'  => Tools::textoBusca(tobj($p, 'texto'))
            ];
        }

        return view('frontend.busca', compact('resultados'));
    }
}
