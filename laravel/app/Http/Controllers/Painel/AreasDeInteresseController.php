<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AreasDeInteresseRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaDeInteresse;

class AreasDeInteresseController extends Controller
{
    public function index()
    {
        $registros = AreaDeInteresse::ordenados()->get();

        return view('painel.areas-de-interesse.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.areas-de-interesse.create');
    }

    public function store(AreasDeInteresseRequest $request)
    {
        try {

            $input = $request->all();

            AreaDeInteresse::create($input);

            return redirect()->route('painel.areas-de-interesse.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AreaDeInteresse $registro)
    {
        return view('painel.areas-de-interesse.edit', compact('registro'));
    }

    public function update(AreasDeInteresseRequest $request, AreaDeInteresse $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.areas-de-interesse.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AreaDeInteresse $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.areas-de-interesse.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
