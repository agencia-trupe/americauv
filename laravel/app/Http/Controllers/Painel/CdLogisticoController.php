<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CdLogisticoRequest;
use App\Http\Controllers\Controller;

use App\Models\CdLogistico;

class CdLogisticoController extends Controller
{
    public function index()
    {
        $registro = CdLogistico::first();

        return view('painel.cd-logistico.edit', compact('registro'));
    }

    public function update(CdLogisticoRequest $request, CdLogistico $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = CdLogistico::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = CdLogistico::upload_imagem_2();

            $registro->update($input);

            return redirect()->route('painel.cd-logistico.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
