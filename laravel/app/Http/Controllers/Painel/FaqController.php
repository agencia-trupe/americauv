<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FaqRequest;
use App\Http\Controllers\Controller;

use App\Models\Pergunta;

class FaqController extends Controller
{
    public function index()
    {
        $registros = Pergunta::ordenados()->get();

        return view('painel.faq.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.faq.create');
    }

    public function store(FaqRequest $request)
    {
        try {

            $input = $request->all();

            Pergunta::create($input);

            return redirect()->route('painel.faq.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pergunta $registro)
    {
        return view('painel.faq.edit', compact('registro'));
    }

    public function update(FaqRequest $request, Pergunta $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.faq.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pergunta $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.faq.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
