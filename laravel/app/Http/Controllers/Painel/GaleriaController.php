<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GaleriaRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;

class GaleriaController extends Controller
{
    public function index()
    {
        $registros = Galeria::ordenados()->get();

        return view('painel.galeria.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.galeria.create');
    }

    public function store(GaleriaRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            Galeria::create($input);

            return redirect()->route('painel.galeria.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Galeria $registro)
    {
        return view('painel.galeria.edit', compact('registro'));
    }

    public function update(GaleriaRequest $request, Galeria $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.galeria.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Galeria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.galeria.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
