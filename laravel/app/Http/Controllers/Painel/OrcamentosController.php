<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OrcamentoRecebido;

class OrcamentosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = OrcamentoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.orcamentos.index', compact('contatosrecebidos'));
    }

    public function show(OrcamentoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.orcamentos.show', compact('contato'));
    }

    public function destroy(OrcamentoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.orcamentos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(OrcamentoRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.orcamentos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
