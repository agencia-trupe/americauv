<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParceirosRequest;
use App\Http\Controllers\Controller;

use App\Models\Parceiros;

class ParceirosController extends Controller
{
    public function index()
    {
        $registro = Parceiros::first();

        return view('painel.parceiros.edit', compact('registro'));
    }

    public function update(ParceirosRequest $request, Parceiros $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Parceiros::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.parceiros.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
