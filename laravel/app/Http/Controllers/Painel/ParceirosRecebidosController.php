<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ParceiroRecebido;

class ParceirosRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ParceiroRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.parceiros.recebidos.index', compact('contatosrecebidos'));
    }

    public function show(ParceiroRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.parceiros.recebidos.show', compact('contato'));
    }

    public function destroy(ParceiroRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.parceiros.recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ParceiroRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.parceiros.recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
