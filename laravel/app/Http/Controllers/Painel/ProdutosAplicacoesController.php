<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoAplicacao;

use App\Helpers\CropImage;

class ProdutosAplicacoesController extends Controller
{
    public function index(Produto $registro)
    {
        $imagens = ProdutoAplicacao::produto($registro->id)->ordenados()->get();

        return view('painel.produtos.aplicacoes.index', compact('imagens', 'registro'));
    }

    public function show(Produto $registro, ProdutoAplicacao $imagem)
    {
        return $imagem;
    }

    public function store(Produto $registro, ProdutosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ProdutoAplicacao::uploadImagem();
            $input['produto_id'] = $registro->id;

            $imagem = ProdutoAplicacao::create($input);

            $view = view('painel.produtos.aplicacoes.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Produto $registro, ProdutoAplicacao $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.produtos.aplicacoes.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Produto $registro)
    {
        try {

            $registro->aplicacoes()->delete();
            return redirect()->route('painel.produtos.aplicacoes.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
