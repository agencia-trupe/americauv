<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoCategoria;
use App\Helpers\Tools;

class ProdutosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ProdutoCategoria::find($filtro)) {
            $registros = Produto::where('produtos_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Produto::leftJoin('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.produtos.create', compact('categorias'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['miniatura'])) $input['miniatura'] = Produto::upload_miniatura();

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();

            if ($request->hasFile('catalogo_pt')) {
                $input['catalogo_pt'] = Tools::fileUpload('catalogo_pt', 'assets/catalogos/');
            }
            if ($request->hasFile('catalogo_en')) {
                $input['catalogo_en'] = Tools::fileUpload('catalogo_en', 'assets/catalogos/');
            }
            if ($request->hasFile('catalogo_es')) {
                $input['catalogo_es'] = Tools::fileUpload('catalogo_es', 'assets/catalogos/');
            }

            Produto::create($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        $categorias = $this->categorias;

        return view('painel.produtos.edit', compact('registro', 'categorias'));
    }

    public function update(ProdutosRequest $request, Produto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['miniatura'])) $input['miniatura'] = Produto::upload_miniatura();

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();

            if ($request->hasFile('catalogo_pt')) {
                $input['catalogo_pt'] = Tools::fileUpload('catalogo_pt', 'assets/catalogos/');
            }
            if ($request->hasFile('catalogo_en')) {
                $input['catalogo_en'] = Tools::fileUpload('catalogo_en', 'assets/catalogos/');
            }
            if ($request->hasFile('catalogo_es')) {
                $input['catalogo_es'] = Tools::fileUpload('catalogo_es', 'assets/catalogos/');
            }

            $registro->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deleteCatalogo(Produto $produto, $lang)
    {
        if (! in_array($lang, ['pt', 'en', 'es'])) abort('404');

        try {

            $produto->update(['catalogo_'.$lang => '']);

            return redirect()->route('painel.produtos.edit', $produto->id)->with('success', 'Catálogo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir catálogo: '.$e->getMessage()]);

        }
    }

}
