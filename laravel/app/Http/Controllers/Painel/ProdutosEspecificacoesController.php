<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspecificacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoEspecificacao;

class ProdutosEspecificacoesController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = ProdutoEspecificacao::produto($produto->id)
            ->ordenados()->get();

        return view('painel.produtos.especificacoes.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.especificacoes.create', compact('produto'));
    }

    public function store(EspecificacoesRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            $produto->especificacoes()->create($input);

            return redirect()->route('painel.produtos.especificacoes.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, ProdutoEspecificacao $registro)
    {
        return view('painel.produtos.especificacoes.edit', compact('registro', 'produto'));
    }

    public function update(EspecificacoesRequest $request, Produto $produto, ProdutoEspecificacao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.produtos.especificacoes.index', $produto)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoEspecificacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.especificacoes.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
