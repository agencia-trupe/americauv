<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SuporteTecnicoRequest;
use App\Http\Controllers\Controller;

use App\Models\SuporteTecnico;

class SuporteTecnicoController extends Controller
{
    public function index()
    {
        $registro = SuporteTecnico::first();

        return view('painel.suporte-tecnico.edit', compact('registro'));
    }

    public function update(SuporteTecnicoRequest $request, SuporteTecnico $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SuporteTecnico::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.suporte-tecnico.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
