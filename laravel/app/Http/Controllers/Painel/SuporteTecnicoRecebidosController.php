<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SuporteTecnicoRecebido;

class SuporteTecnicoRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = SuporteTecnicoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.suporte-tecnico.recebidos.index', compact('contatosrecebidos'));
    }

    public function show(SuporteTecnicoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.suporte-tecnico.recebidos.show', compact('contato'));
    }

    public function destroy(SuporteTecnicoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.suporte-tecnico.recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(SuporteTecnicoRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.suporte-tecnico.recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
