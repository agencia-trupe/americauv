<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $contatosrecebidos = TrabalheConosco::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.trabalhe-conosco.index', compact('contatosrecebidos'));
    }

    public function show(TrabalheConosco $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.trabalhe-conosco.show', compact('contato'));
    }

    public function destroy(TrabalheConosco $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.trabalhe-conosco.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(TrabalheConosco $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contato.trabalhe-conosco.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
