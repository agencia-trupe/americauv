<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ParceirosRecebidosRequest;

use App\Models\Contato;
use App\Models\Parceiros;
use App\Models\ParceiroRecebido;

use App\Helpers\Tools;

use Illuminate\Support\Facades\Mail;

class ParceirosController extends Controller
{
    public function index()
    {
        $parceiros = Parceiros::first();

        return view('frontend.parceiros', compact('parceiros'));
    }

    public function post(ParceirosRecebidosRequest $request, ParceiroRecebido $contatoRecebido)
    {
        $data = $request->all();
        $data['apresentacao'] = Tools::fileUpload('apresentacao', 'apresentacoes-parceiros//');

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect()->route('parceiros')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.parceiro', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[PARCEIRO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
