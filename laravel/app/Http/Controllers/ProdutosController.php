<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrcamentosRecebidosRequest;

use App\Models\ProdutoCategoria;
use App\Models\Produto;
use App\Models\OrcamentoRecebido;
use App\Models\Contato;

use Illuminate\Support\Facades\Mail;

class ProdutosController extends Controller
{
    public function tecnologias(ProdutoCategoria $categoria)
    {
        view()->share('tecnologiaSlug', $categoria->slug);

        return view('frontend.produtos.index', compact('categoria'));
    }

    public function tecnologiasShow(ProdutoCategoria $categoria, Produto $produto)
    {
        view()->share('tecnologiaSlug', $categoria->slug);

        if ($produto->categoria->slug != $categoria->slug) abort('404');

        return view('frontend.produtos.show', compact('produto'));
    }

    public function tintas()
    {
        $categoria = ProdutoCategoria::whereSlug('tinta-digital-uv-led-hnd-ink')
            ->firstOrFail();

        return view('frontend.produtos.index', compact('categoria'));
    }

    public function tintasShow(Produto $produto)
    {
        if ($produto->categoria->slug != 'tinta-digital-uv-led-hnd-ink') abort('404');

        return view('frontend.produtos.show', compact('produto'));
    }

    public function materiais()
    {
        $categoria = ProdutoCategoria::whereSlug('materiais')
            ->firstOrFail();

        return view('frontend.produtos.index', compact('categoria'));
    }

    public function materiaisShow(Produto $produto)
    {
        if ($produto->categoria->slug != 'materiais') abort('404');

        return view('frontend.produtos.show', compact('produto'));
    }

    public function orcamento(Produto $produto)
    {
        return view('frontend.produtos.orcamento', compact('produto'));
    }

    public function orcamentoPost(OrcamentosRecebidosRequest $request, OrcamentoRecebido $orcamento)
    {
        $data = $request->all();

        $orcamento->create($data);
        $this->sendMail($data);

        $response = ['message' => t('produtos.sucesso')];

        return response()->json($response);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.orcamento', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[ORÇAMENTO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
