<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\SuporteTecnicoRecebidosRequest;

use App\Models\SuporteTecnico;
use App\Models\Assunto;
use App\Models\Pergunta;
use App\Models\Contato;

use Illuminate\Support\Facades\Mail;
use App\Models\SuporteTecnicoRecebido;

class SuporteController extends Controller
{
    public function index()
    {
        $suporte  = SuporteTecnico::first();
        $assuntos = Assunto::ordenados()->get();
        $faq      = Pergunta::ordenados()->get();

        return view('frontend.suporte-tecnico', compact('suporte', 'assuntos', 'faq'));
    }

    public function post(SuporteTecnicoRecebidosRequest $request, SuporteTecnicoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect()->route('suporte')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.suporte', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[SUPORTE TÉCNICO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
