<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\TrabalheConoscoRequest;

use App\Models\Contato;
use App\Models\TrabalheConosco;
use App\Models\AreaDeInteresse;

use App\Helpers\Tools;

use Illuminate\Support\Facades\Mail;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();
        $areas   = AreaDeInteresse::ordenados()->get();

        return view('frontend.trabalhe-conosco', compact('contato', 'areas'));
    }

    public function post(TrabalheConoscoRequest $request, TrabalheConosco $contatoRecebido)
    {
        $data = $request->all();
        $data['curriculo'] = Tools::fileUpload('curriculo', 'curriculos/');

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect()->route('trabalhe')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.trabalhe', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[TRABALHE CONOSCO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
