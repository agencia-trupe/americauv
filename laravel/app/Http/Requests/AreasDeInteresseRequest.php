<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreasDeInteresseRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome_pt' => 'required',
            'nome_en' => 'required',
            'nome_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
