<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'    => 'required|image',
            'link'      => '',
            'botao_pt'  => 'required_with:link',
            'botao_en'  => 'required_with:link',
            'botao_es'  => 'required_with:link',
            'botao_cor' => 'required'
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
