<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CdLogisticoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1_pt' => 'required',
            'texto_1_en' => 'required',
            'texto_1_es' => 'required',
            'texto_2_pt' => 'required',
            'texto_2_en' => 'required',
            'texto_2_es' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
        ];
    }
}
