<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
