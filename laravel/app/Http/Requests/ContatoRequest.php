<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                     => 'email|required',
            'whatsapp'                  => 'required',
            'telefones'                 => 'required',
            'endereco'                  => 'required',
            'google_maps'               => 'required',
            'texto_contato_pt'          => 'required',
            'texto_contato_en'          => 'required',
            'texto_contato_es'          => 'required',
            'texto_trabalhe_conosco_pt' => 'required',
            'texto_trabalhe_conosco_en' => 'required',
            'texto_trabalhe_conosco_es' => 'required',
            'imagem'                    => 'image',
        ];
    }
}
