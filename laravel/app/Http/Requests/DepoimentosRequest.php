<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'empresa' => 'required',
            'imagem' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
