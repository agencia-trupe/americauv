<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EspecificacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'especificacao_pt' => 'required',
            'especificacao_en' => 'required',
            'especificacao_es' => 'required',
            'valor_pt' => 'required',
            'valor_en' => 'required',
            'valor_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
