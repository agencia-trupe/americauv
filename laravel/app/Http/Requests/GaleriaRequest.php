<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GaleriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'subtitulo_es' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'capa' => 'required|image',
            'video_pt' => '',
            'video_en' => '',
            'video_es' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
