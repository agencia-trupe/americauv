<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'imagem' => 'image',
            'missao_pt' => 'required',
            'missao_en' => 'required',
            'missao_es' => 'required',
            'visao_pt' => 'required',
            'visao_en' => 'required',
            'visao_es' => 'required',
            'valores_pt' => 'required',
            'valores_en' => 'required',
            'valores_es' => 'required',
            'video' => '',
        ];
    }
}
