<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => t('footer.email-required'),
            'email.email'    => t('footer.email-email'),
            'email.unique'   => t('footer.email-unique')
        ];
    }
}
