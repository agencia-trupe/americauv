<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrcamentosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'produto'  => 'required',
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => t('contato.erro'),
            'email'    => t('contato.erro'),
        ];
    }
}
