<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ParceirosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'         => 'required',
            'empresa'      => 'required',
            'email'        => 'required|email',
            'mensagem'     => 'required',
            'apresentacao' => 'required|max:4000|mimes:doc,docx,pdf'
        ];
    }

    public function messages()
    {
        return [
            'apresentacao.required' => t('contato.arquivo-required'),
            'apresentacao.mimes'    => t('contato.arquivo-mimes'),
            'apresentacao.max'      => t('contato.arquivo-max')
        ];
    }
}
