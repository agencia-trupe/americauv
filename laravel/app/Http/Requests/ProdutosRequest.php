<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'subtitulo_es' => 'required',
            'miniatura' => 'required|image',
            'capa' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'catalogo_pt' => '',
            'catalogo_en' => '',
            'catalogo_es' => '',
            'video_pt' => '',
            'video_en' => '',
            'video_es' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['miniatura'] = 'image';
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
