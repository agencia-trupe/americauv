<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TrabalheConoscoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'              => 'required',
            'email'             => 'required|email',
            'area_de_interesse' => 'required',
            'mensagem'          => 'required',
            'curriculo'         => 'required|max:4096|mimes:doc,docx,pdf'
        ];
    }

    public function messages()
    {
        return [
            'curriculo.required' => t('contato.arquivo-required'),
            'curriculo.mimes'    => t('contato.arquivo-mimes'),
            'curriculo.max'      => t('contato.arquivo-max')
        ];
    }
}
