<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('busca', 'HomeController@busca')->name('busca');

    Route::get('a-america-uv/institucional', 'EmpresaController@institucional')->name('empresa.institucional');
    Route::get('a-america-uv/cd-logistico', 'EmpresaController@cdLogistico')->name('empresa.cd-logistico');
    Route::get('a-america-uv/galeria', 'EmpresaController@galeria')->name('empresa.galeria');
    Route::get('a-america-uv/galeria/{galeria_slug}', 'EmpresaController@galeriaShow')->name('empresa.galeria.show');

    Route::get('tecnologias/{categoria_slug}', 'ProdutosController@tecnologias')->name('tecnologias');
    Route::get('tecnologias/{categoria_slug}/{produto_slug}', 'ProdutosController@tecnologiasShow')->name('tecnologias.show');

    Route::get('tinta-digital-uv-led-hnd-ink', 'ProdutosController@tintas')->name('tinta-digital-uv-led-hnd-ink');
    Route::get('tinta-digital-uv-led-hnd-ink/{produto_slug}', 'ProdutosController@tintasShow')->name('tinta-digital-uv-led-hnd-ink.show');

    Route::get('materiais', 'ProdutosController@materiais')->name('materiais');
    Route::get('materiais/{produto_slug}', 'ProdutosController@materiaisShow')->name('materiais.show');

    Route::get('orcamento/{produto_slug}', 'ProdutosController@orcamento')->name('orcamento');
    Route::post('orcamento', 'ProdutosController@orcamentoPost')->name('orcamento.post');

    Route::get('suporte-tecnico', 'SuporteController@index')->name('suporte');
    Route::post('suporte-tecnico', 'SuporteController@post')->name('suporte.post');

    Route::get('parceiros', 'ParceirosController@index')->name('parceiros');
    Route::post('parceiros', 'ParceirosController@post')->name('parceiros.post');

    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    Route::get('trabalhe-conosco', 'TrabalheConoscoController@index')->name('trabalhe');
    Route::post('trabalhe-conosco', 'TrabalheConoscoController@post')->name('trabalhe.post');


    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->intended();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('produtos.especificacoes', 'ProdutosEspecificacoesController', ['parameters' => ['especificacoes' => 'especificacoes_produtos']]);
		Route::resource('produtos/categorias', 'ProdutosCategoriasController', ['except' => ['create', 'store', 'destroy'], 'parameters' => ['categorias' => 'categorias_produtos']]);
		Route::resource('produtos', 'ProdutosController');
		Route::get('produtos/{produtos}/imagens/clear', [
			'as'   => 'painel.produtos.imagens.clear',
			'uses' => 'ProdutosImagensController@clear'
		]);
        Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
        Route::get('produtos/{produtos}/aplicacoes/clear', [
			'as'   => 'painel.produtos.aplicacoes.clear',
			'uses' => 'ProdutosAplicacoesController@clear'
		]);
        Route::resource('produtos.aplicacoes', 'ProdutosAplicacoesController', ['parameters' => ['aplicacoes' => 'aplicacoes_produtos']]);
        Route::get('produtos/{produtos}/catalogo/delete/{lang}', 'ProdutosController@deleteCatalogo')->name('painel.produtos.deleteCatalogo');

        Route::get('parceiros/recebidos/{parceiros_recebidos}/toggle', ['as' => 'painel.parceiros.recebidos.toggle', 'uses' => 'ParceirosRecebidosController@toggle']);
        Route::resource('parceiros/recebidos', 'ParceirosRecebidosController', ['parameters' => ['recebidos' => 'parceiros_recebidos']]);
        Route::resource('parceiros', 'ParceirosController', ['only' => ['index', 'update']]);

		Route::resource('faq', 'FaqController');
        Route::resource('assuntos', 'AssuntosController');

        Route::get('suporte-tecnico/recebidos/{suporte_recebidos}/toggle', ['as' => 'painel.suporte-tecnico.recebidos.toggle', 'uses' => 'SuporteTecnicoRecebidosController@toggle']);
        Route::resource('suporte-tecnico/recebidos', 'SuporteTecnicoRecebidosController', ['parameters' => ['recebidos' => 'suporte_recebidos']]);
		Route::resource('suporte-tecnico', 'SuporteTecnicoController', ['only' => ['index', 'update']]);

        Route::resource('galeria', 'GaleriaController');
		Route::get('galeria/{galeria}/imagens/clear', [
			'as'   => 'painel.galeria.imagens.clear',
			'uses' => 'GaleriaImagensController@clear'
		]);
		Route::resource('galeria.imagens', 'GaleriaImagensController', ['parameters' => ['imagens' => 'imagens_galeria']]);

        Route::resource('cd-logistico', 'CdLogisticoController', ['only' => ['index', 'update']]);
		Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);

        Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('chamadas', 'ChamadasController');
		Route::resource('banners', 'BannersController');
		Route::resource('areas-de-interesse', 'AreasDeInteresseController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');

        Route::get('contato/trabalhe-conosco/{trabalhe_conosco}/toggle', ['as' => 'painel.contato.trabalhe-conosco.toggle', 'uses' => 'TrabalheConoscoController@toggle']);
        Route::resource('contato/trabalhe-conosco', 'TrabalheConoscoController');

        Route::resource('contato', 'ContatoController');

        Route::resource('newsletter', 'NewsletterController', [
            'only' => ['index', 'destroy']
        ]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')->name('painel.newsletter.exportar');

        Route::get('orcamentos/{orcamentos}/toggle', ['as' => 'painel.orcamentos.toggle', 'uses' => 'OrcamentosController@toggle']);
        Route::resource('orcamentos', 'OrcamentosController');

        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
