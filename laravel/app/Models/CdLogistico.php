<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CdLogistico extends Model
{
    protected $table = 'cd_logistico';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/cd-logistico/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/cd-logistico/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('texto_1_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('texto_2_'.app()->getLocale(), 'LIKE', "%{$termo}%");
    }
}
