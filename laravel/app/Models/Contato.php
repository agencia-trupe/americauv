<?php

namespace App\Models;

use App\Helpers\CropImage;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 590,
            'height' => null,
            'path'   => 'assets/img/contato/'
        ]);
    }
}
