<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('texto_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('missao_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('visao_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('valores_'.app()->getLocale(), 'LIKE', "%{$termo}%");
    }

}
