<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletter';

    protected $fillable = ['nome', 'email'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
    }
}
