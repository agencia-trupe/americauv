<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Parceiros extends Model
{
    protected $table = 'parceiros';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 590,
            'height' => null,
            'path'   => 'assets/img/parceiros/'
        ]);
    }

}
