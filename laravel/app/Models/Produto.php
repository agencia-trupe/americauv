<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public function aplicacoes()
    {
        return $this->hasMany('App\Models\ProdutoAplicacao', 'produto_id')->ordenados();
    }

    public function especificacoes()
    {
        return $this->hasMany('App\Models\ProdutoEspecificacao', 'produto_id')->ordenados();
    }

    public static function upload_miniatura()
    {
        return CropImage::make('miniatura', [
            'width'       => 285,
            'height'      => 160,
            'transparent' => true,
            'upsize'      => true,
            'path'        => 'assets/img/produtos/miniatura/'
        ]);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'       => 1920,
            'height'      => 620,
            'path'        => 'assets/img/produtos/'
        ]);
    }

    public function getShowLinkAttribute()
    {
        if (in_array($this->categoria->slug, ['materiais', 'tinta-digital-uv-led-hnd-ink'])) {
            return route($this->categoria->slug.'.show', $this->slug);
        }

        return route('tecnologias.show', [$this->categoria->slug, $this->slug]);
    }

    public function relacionados()
    {
        return self::
            where('produtos_categoria_id', $this->produtos_categoria_id)
            ->where('id', '!=', $this->id)
            ->ordenados()->get()->take(4);
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('subtitulo_'.app()->getLocale(), 'LIKE', "%{$termo}%")
            ->orWhere('texto_'.app()->getLocale(), 'LIKE', "%{$termo}%");
    }
}
