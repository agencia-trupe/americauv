<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SuporteTecnico extends Model
{
    protected $table = 'suporte_tecnico';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 590,
            'height' => null,
            'path'   => 'assets/img/suporte-tecnico/'
        ]);
    }

}
