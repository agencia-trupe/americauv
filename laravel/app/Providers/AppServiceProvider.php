<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('orcamentosNaoLidos', \App\Models\OrcamentoRecebido::naoLidos()->count());
            $view->with('curriculosNaoLidos', \App\Models\TrabalheConosco::naoLidos()->count());
            $view->with('suporteNaoLidos', \App\Models\SuporteTecnicoRecebido::naoLidos()->count());
            $view->with('parceirosNaoLidos', \App\Models\ParceiroRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
