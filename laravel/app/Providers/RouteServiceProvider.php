<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('produtos', 'App\Models\Produto');
		$router->model('categorias_produtos', 'App\Models\ProdutoCategoria');
		$router->model('imagens_produtos', 'App\Models\ProdutoImagem');
		$router->model('aplicacoes_produtos', 'App\Models\ProdutoAplicacao');
		$router->model('especificacoes_produtos', 'App\Models\ProdutoEspecificacao');
		$router->model('parceiros', 'App\Models\Parceiros');
		$router->model('parceiros_recebidos', 'App\Models\ParceiroRecebido');
		$router->model('faq', 'App\Models\Pergunta');
		$router->model('assuntos', 'App\Models\Assunto');
		$router->model('suporte-tecnico', 'App\Models\SuporteTecnico');
		$router->model('suporte_recebidos', 'App\Models\SuporteTecnicoRecebido');
		$router->model('galeria', 'App\Models\Galeria');
		$router->model('imagens_galeria', 'App\Models\GaleriaImagem');
		$router->model('cd-logistico', 'App\Models\CdLogistico');
		$router->model('institucional', 'App\Models\Institucional');
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('home', 'App\Models\Home');
		$router->model('chamadas', 'App\Models\Chamada');
		$router->model('banners', 'App\Models\Banner');
		$router->model('areas-de-interesse', 'App\Models\AreaDeInteresse');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('orcamentos', 'App\Models\OrcamentoRecebido');
        $router->model('trabalhe_conosco', 'App\Models\TrabalheConosco');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('galeria_slug', function($key) {
            return \App\Models\Galeria::whereSlug($key)->firstOrFail();
        });
        $router->bind('categoria_slug', function($key) {
            return \App\Models\ProdutoCategoria::whereSlug($key)->firstOrFail();
        });
        $router->bind('produto_slug', function($key) {
            return \App\Models\Produto::whereSlug($key)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
