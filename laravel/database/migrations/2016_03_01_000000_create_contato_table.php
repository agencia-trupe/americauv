<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('whatsapp');
            $table->text('telefones');
            $table->text('endereco');
            $table->text('google_maps');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('youtube');
            $table->string('linkedin');
            $table->string('twitter');
            $table->text('texto_contato_pt');
            $table->text('texto_contato_en');
            $table->text('texto_contato_es');
            $table->text('texto_trabalhe_conosco_pt');
            $table->text('texto_trabalhe_conosco_en');
            $table->text('texto_trabalhe_conosco_es');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
