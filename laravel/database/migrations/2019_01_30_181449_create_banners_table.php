<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('link');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('botao_pt');
            $table->string('botao_en');
            $table->string('botao_es');
            $table->string('botao_cor');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
