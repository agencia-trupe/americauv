<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('imagem');
            $table->text('missao_pt');
            $table->text('missao_en');
            $table->text('missao_es');
            $table->text('visao_pt');
            $table->text('visao_en');
            $table->text('visao_es');
            $table->text('valores_pt');
            $table->text('valores_en');
            $table->text('valores_es');
            $table->string('video_pt');
            $table->string('video_en');
            $table->string('video_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
