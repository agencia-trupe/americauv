<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdLogisticoTable extends Migration
{
    public function up()
    {
        Schema::create('cd_logistico', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1_pt');
            $table->text('texto_1_en');
            $table->text('texto_1_es');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->text('texto_2_es');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cd_logistico');
    }
}
