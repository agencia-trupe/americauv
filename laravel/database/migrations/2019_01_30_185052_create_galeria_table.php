<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriaTable extends Migration
{
    public function up()
    {
        Schema::create('galeria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('subtitulo_pt');
            $table->string('subtitulo_en');
            $table->string('subtitulo_es');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('capa');
            $table->string('video_pt');
            $table->string('video_en');
            $table->string('video_es');
            $table->timestamps();
        });

        Schema::create('galeria_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('galeria_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('galeria_id')->references('id')->on('galeria')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('galeria_imagens');
        Schema::drop('galeria');
    }
}
