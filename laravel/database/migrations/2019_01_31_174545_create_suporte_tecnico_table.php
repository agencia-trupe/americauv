<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuporteTecnicoTable extends Migration
{
    public function up()
    {
        Schema::create('suporte_tecnico', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('suporte_tecnico');
    }
}
