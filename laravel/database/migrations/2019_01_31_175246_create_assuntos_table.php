<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssuntosTable extends Migration
{
    public function up()
    {
        Schema::create('assuntos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome_pt');
            $table->string('nome_en');
            $table->string('nome_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('assuntos');
    }
}
