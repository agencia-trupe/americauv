<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration
{
    public function up()
    {
        Schema::create('faq', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('pergunta_pt');
            $table->string('pergunta_en');
            $table->string('pergunta_es');
            $table->text('resposta_pt');
            $table->text('resposta_en');
            $table->text('resposta_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('faq');
    }
}
