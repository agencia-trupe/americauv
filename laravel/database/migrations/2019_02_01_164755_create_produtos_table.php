<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->text('subtitulo_pt');
            $table->text('subtitulo_en');
            $table->text('subtitulo_es');
            $table->string('miniatura');
            $table->string('capa');
            $table->string('cor_texto_capa');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('catalogo_pt');
            $table->string('catalogo_en');
            $table->string('catalogo_es');
            $table->string('video_pt');
            $table->string('video_en');
            $table->string('video_es');
            $table->foreign('produtos_categoria_id')->references('id')->on('produtos_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });

        Schema::create('produtos_aplicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });

        Schema::create('produtos_especificacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('especificacao_pt');
            $table->string('especificacao_en');
            $table->string('especificacao_es');
            $table->string('valor_pt');
            $table->string('valor_en');
            $table->string('valor_es');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('produtos_especificacoes');
        Schema::drop('produtos_aplicacoes');
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
        Schema::drop('produtos_categorias');
    }
}
