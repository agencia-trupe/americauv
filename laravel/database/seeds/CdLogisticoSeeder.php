<?php

use Illuminate\Database\Seeder;

class CdLogisticoSeeder extends Seeder
{
    public function run()
    {
        DB::table('cd_logistico')->insert([
            'texto_1_pt' => '',
            'texto_1_en' => '',
            'texto_1_es' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'texto_2_es' => '',
            'imagem_1' => '',
            'imagem_2' => '',
        ]);
    }
}
