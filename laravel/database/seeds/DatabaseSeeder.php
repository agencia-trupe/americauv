<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(ParceirosSeeder::class);
		$this->call(SuporteTecnicoSeeder::class);
		$this->call(CdLogisticoSeeder::class);
		$this->call(InstitucionalSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ProdutosCategoriasTableSeeder::class);

        Model::reguard();
    }
}
