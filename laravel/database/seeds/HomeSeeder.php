<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'titulo_pt' => '',
            'titulo_en' => '',
            'titulo_es' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
        ]);
    }
}
