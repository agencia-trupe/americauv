<?php

use Illuminate\Database\Seeder;

class InstitucionalSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
            'imagem' => '',
            'missao_pt' => '',
            'missao_en' => '',
            'missao_es' => '',
            'visao_pt' => '',
            'visao_en' => '',
            'visao_es' => '',
            'valores_pt' => '',
            'valores_en' => '',
            'valores_es' => '',
            'video_pt' => '',
            'video_en' => '',
            'video_es' => '',
        ]);
    }
}
