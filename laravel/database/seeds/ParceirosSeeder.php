<?php

use Illuminate\Database\Seeder;

class ParceirosSeeder extends Seeder
{
    public function run()
    {
        DB::table('parceiros')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
            'imagem' => '',
        ]);
    }
}
