<?php

use Illuminate\Database\Seeder;

class ProdutosCategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos_categorias')->insert([
            [
                'ordem' => 1,
                'titulo' => 'Impressoras para rígidos',
                'slug'   => 'impressoras-para-rigidos',
            ],
            [
                'ordem' => 2,
                'titulo' => 'Impressoras para flexíveis',
                'slug'   => 'impressoras-para-flexiveis',
            ],
            [
                'ordem' => 3,
                'titulo' => 'Impressoras Híbridas',
                'slug'   => 'impressoras-hibridas',
            ],
            [
                'ordem' => 4,
                'titulo' => 'Sistemas de Corte',
                'slug'   => 'sistemas-de-corte',
            ],
            [
                'ordem' => 5,
                'titulo' => 'Tinta Digital UV LED HND-INK',
                'slug'   => 'tinta-digital-uv-led-hnd-ink',
            ],
            [
                'ordem' => 6,
                'titulo' => 'Materiais',
                'slug'   => 'materiais',
            ],
        ]);
    }
}
