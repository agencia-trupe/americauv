<?php

use Illuminate\Database\Seeder;

class SuporteTecnicoSeeder extends Seeder
{
    public function run()
    {
        DB::table('suporte_tecnico')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
            'imagem' => '',
        ]);
    }
}
