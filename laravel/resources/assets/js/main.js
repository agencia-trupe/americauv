import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.busca-handle').click(function(event) {
    event.preventDefault();

    $('.modal-busca').fadeIn(function() {
        $('.modal-busca input[type=text]').focus();
    });

    event.stopPropagation();
});

$('body').on('click', function(event) {
    if (!$(event.target).hasClass('modal-busca') && !$(event.target).parents('.modal-busca').length && $('.modal-busca:visible').length) {
        $('.modal-busca').fadeOut();
    }
});

$('#form-newsletter').submit(function(event) {
    event.preventDefault();

    var $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: {
            email: $form.find('input[name=newsletter_email]').val(),
        }
    }).done(function(data) {
        alert(data.message);
        $form[0].reset();
    }).fail(function(data) {
        var res = data.responseJSON,
            txt = res.email;
        alert(txt);
    }).always(function() {
        $form.removeClass('sending');
    });
});

$('.banners').cycle({
    slides: '>.banner',
    pagerTemplate: '<a href="#">{{slideNum}}</a>'
});

$('.depoimentos-cycle').cycle({
    slides: '>.depoimento',
    next: '>.next',
    autoHeight: 'calc',
    timeout: 0
});

$('.faq a').click(function(e) {
    e.preventDefault();

    $(this).toggleClass('open').next().slideToggle();
});

if ($('.input-arquivo').length) {
    var textoPadrao = $('.input-arquivo span').text();

    $('.input-arquivo input').change(function(event) {
        if (event.target.files[0]) {
            $('.input-arquivo span').text(event.target.files[0].name)
                .addClass('ativo');
        } else {
            $('.input-arquivo span').text(textoPadrao).removeClass('ativo');
        }
    });
}

$('.btn-topo').click(function(e) {
    e.preventDefault();

    $('html, body').animate({
        scrollTop: 0
    });
});

$('.lightbox-content').fancybox({
    helpers: {
        overlay: {
            locked: false,
            css: {'background-color': 'rgba(0,0,0,.5)'},
        }
    },
    type: 'ajax',
    maxWidth: 840,
    maxHeight: '95%',
    padding: 0
});

$(document).on('submit', '#form-orcamento-lightbox', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-orcamento-lightbox-response');

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $response.fadeOut('fast');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/orcamento',
        data: {
            produto: $('#l_produto').val(),
            nome: $('#l_nome').val(),
            email: $('#l_email').val(),
            telefone: $('#l_telefone').val(),
            mensagem: $('#l_mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
            $.fancybox.update();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
                $.fancybox.update();
            }
        },
        dataType: 'json'
    }).always(function() {
        $form.removeClass('sending');
    });
});
