<?php

return [

    'nav' => [
        'empresa'                    => 'Company',
        'america-uv'                 => 'America UV',
        'institucional'              => 'Institutional',
        'cd-logistico'               => 'Logistic CD',
        'galeria'                    => 'Gallery',
        'produtos'                   => 'Products',
        'tecnologias'                => 'Technology',
        'impressoras-para-rigidos'   => 'Printers for rigid',
        'impressoras-para-flexiveis' => 'Printers for flexible',
        'impressoras-hibridas'       => 'Hybrid Printers',
        'sistemas-de-corte'          => 'Cutting Systems',
        'tinta-digital-uv-led-hnd-ink' => 'UV LED HND-INK Digital Ink',
        'materiais'                  => 'Materials',
        'suporte-tecnico'            => 'Technical support',
        'suporte'                    => 'Support',
        'parceiros'                  => 'Partners',
        'contato'                    => 'Contact',
        'trabalhe-conosco'           => 'Work with us',
        'novidades'                  => 'News',
        'buscar'                     => 'search'
    ],

    '404' => 'Page not found',

    'footer' => [
        'novidades'      => 'Subscribe',
        'siga-nos'       => 'Follow Us',
        'email-required' => 'E-mail requested.',
        'email-email'    => 'Please enter a valid email.',
        'email-unique'   => 'This email is already registered.',
        'success'        => 'Registration successfully complete!',
        'copyright'      => 'All rights reserved',
        'sites'          => 'Website creation'
    ],

    'empresa' => [
        'missao'         => 'Mission',
        'visao'          => 'View',
        'valores'        => 'Values',
        'galeria-voltar' => 'back to gallery home'
    ],

    'contato' => [
        'fale-conosco'      => 'Contact us',
        'nome'              => 'name',
        'empresa'           => 'company',
        'telefone'          => 'voice',
        'assunto'           => 'subject - SELECT...',
        'mensagem'          => 'message',
        'enviar'            => 'submit',
        'erro'              => 'Complete all the fields correctly.',
        'sucesso'           => 'Message sent successfully!',
        'area-de-interesse' => 'AREA OF INTEREST',
        'curriculo'         => 'ATTACH CURRICULUM',
        'arquivo-required'  => 'Please enter a file.',
        'arquivo-mimes'     => 'The file must be of type: DOC, DOCX or PDF.',
        'arquivo-max'       => 'The file should not exceed 4MB',
        'quero-parceiro'    => 'I want to be a partner',
        'apresentacao'      => 'ATTACH COMPANY PRESENTATION'
    ],

    'produtos' => [
        'saiba-mais'          => 'Know more',
        'catalogo'            => 'Download<br>Catalog',
        'solicitar-orcamento' => 'Request a<br>Quote',
        'especificacoes'      => 'Technical Specifications',
        'aplicacoes'          => 'Applications',
        'relacionados'        => 'Related products',
        'topo'                => 'Top',
        'orcamento-info'      => 'Budget | More information',
        'sucesso'             => 'Budget sent successfully!'
    ],

    'busca' => [
        'titulo' => 'Search result',
        'nenhum' => 'No results found.'
    ]

];
