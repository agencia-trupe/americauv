<?php

return [

    'nav' => [
        'empresa'                    => 'Negocios',
        'america-uv'                 => 'America UV',
        'institucional'              => 'Institucional',
        'cd-logistico'               => 'CD Logístico',
        'galeria'                    => 'Galería',
        'produtos'                   => 'Productos',
        'tecnologias'                => 'Tecnologías',
        'impressoras-para-rigidos'   => 'Impresoras rígidas',
        'impressoras-para-flexiveis' => 'Impresoras flexibles',
        'impressoras-hibridas'       => 'Impresoras Híbridas',
        'sistemas-de-corte'          => 'Sistemas de Corte',
        'tinta-digital-uv-led-hnd-ink' => 'Tinta Digital UV LED HND-INK',
        'materiais'                  => 'Materiales',
        'suporte-tecnico'            => 'Soporte técnico',
        'suporte'                    => 'Soporte',
        'parceiros'                  => 'Socios',
        'contato'                    => 'Contacto',
        'trabalhe-conosco'           => 'Trabaja con nosotros',
        'novidades'                  => 'Noticias',
        'buscar'                     => 'buscar'
    ],

    '404' => 'Página no encontrada',

    'footer' => [
        'novidades'      => 'Quiero recibir novedades',
        'siga-nos'       => 'Síguenos',
        'email-required' => 'Llene su e-mail.',
        'email-email'    => 'Introduzca un e-mail válido.',
        'email-unique'   => 'Este e-mail ya está registrado.',
        'success'        => '¡El registro efectuado con éxito!',
        'copyright'      => 'Todos los derechos reservados',
        'sites'          => 'Creación de sitios'
    ],

    'empresa' => [
        'missao'         => 'Misión',
        'visao'          => 'Vista',
        'valores'        => 'Valores',
        'galeria-voltar' => 'volver a la casilla de la Galería'
    ],

    'contato' => [
        'fale-conosco'      => 'Hable con nosotros',
        'nome'              => 'nombre',
        'empresa'           => 'negocios',
        'telefone'          => 'teléfono',
        'assunto'           => 'tema - SELECCIONE...',
        'mensagem'          => 'mensaje',
        'enviar'            => 'enviar',
        'erro'              => 'Rellene todos los campos correctamente.',
        'sucesso'           => 'Mensaje enviado correctamente!',
        'area-de-interesse' => 'AREA DE INTERES',
        'curriculo'         => 'ANEXAR CURRÍCULO',
        'arquivo-required'  => 'Inserte un archivo.',
        'arquivo-mimes'     => 'El archivo debe ser del tipo: DOC, DOCX o PDF.',
        'arquivo-max'       => 'El archivo no debe exceder de 4 MB',
        'quero-parceiro'    => 'Quiero ser socio',
        'apresentacao'      => 'ANEXAR PRESENTACIÓN DE LA EMPRESA'
    ],

    'produtos' => [
        'saiba-mais'          => 'Sepa mas',
        'catalogo'            => 'Descargar el<br>Catálogo',
        'solicitar-orcamento' => 'Solicitar<br>Presupuesto',
        'especificacoes'      => 'Especificaciones tecnicas',
        'aplicacoes'          => 'Aplicaciones',
        'relacionados'        => 'Productos relacionados',
        'topo'                => 'Superior',
        'orcamento-info'      => 'Presupuesto | Mas informaciones',
        'sucesso'             => '¡Presupuesto enviado con éxito!'
    ],

    'busca' => [
        'titulo' => 'Resultado de la búsqueda',
        'nenhum' => 'Ningún resultado encontrado.'
    ]

];
