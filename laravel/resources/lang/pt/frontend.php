<?php

return [

    'nav' => [
        'empresa'                    => 'Empresa',
        'america-uv'                 => 'A America UV',
        'institucional'              => 'Institucional',
        'cd-logistico'               => 'CD Logístico',
        'galeria'                    => 'Galeria',
        'produtos'                   => 'Produtos',
        'tecnologias'                => 'Tecnologias',
        'impressoras-para-rigidos'   => 'Impressoras para rígidos',
        'impressoras-para-flexiveis' => 'Impressoras para flexíveis',
        'impressoras-hibridas'       => 'Impressoras Híbridas',
        'sistemas-de-corte'          => 'Sistemas de Corte',
        'tinta-digital-uv-led-hnd-ink' => 'Tinta Digital UV LED HND-INK',
        'materiais'                  => 'Materiais',
        'suporte-tecnico'            => 'Suporte Técnico',
        'suporte'                    => 'Suporte',
        'parceiros'                  => 'Parceiros',
        'contato'                    => 'Contato',
        'trabalhe-conosco'           => 'Trabalhe Conosco',
        'novidades'                  => 'Novidades',
        'buscar'                     => 'buscar'
    ],

    '404' => 'Página não encontrada',

    'footer' => [
        'novidades'      => 'Quero receber novidades',
        'siga-nos'       => 'Siga-nos',
        'email-required' => 'Preencha seu e-mail.',
        'email-email'    => 'Insira um e-mail válido.',
        'email-unique'   => 'Este e-mail já está cadastrado.',
        'success'        => 'Cadastro efetuado com sucesso!',
        'copyright'      => 'Todos os direitos reservados',
        'sites'          => 'Criação de sites'
    ],

    'empresa' => [
        'missao'         => 'Missão',
        'visao'          => 'Visão',
        'valores'        => 'Valores',
        'galeria-voltar' => 'voltar para a home da Galeria'
    ],

    'contato' => [
        'fale-conosco'      => 'Fale Conosco',
        'nome'              => 'nome',
        'empresa'           => 'empresa',
        'telefone'          => 'telefone',
        'assunto'           => 'assunto - SELECIONE...',
        'mensagem'          => 'mensagem',
        'enviar'            => 'enviar',
        'erro'              => 'Preencha todos os campos corretamente.',
        'sucesso'           => 'Mensagem enviada com sucesso!',
        'area-de-interesse' => 'ÁREA DE INTERESSE',
        'curriculo'         => 'ANEXAR CURRÍCULO',
        'arquivo-required'  => 'Insira um arquivo.',
        'arquivo-mimes'     => 'O arquivo deve ser do tipo: DOC, DOCX ou PDF.',
        'arquivo-max'       => 'O arquivo não deve exceder 4MB',
        'quero-parceiro'    => 'Quero ser parceiro',
        'apresentacao'      => 'ANEXAR APRESENTAÇÃO DA EMPRESA'
    ],

    'produtos' => [
        'saiba-mais'          => 'Saiba Mais',
        'catalogo'            => 'Download do<br>Catálogo',
        'solicitar-orcamento' => 'Solicitar<br>Orçamento',
        'especificacoes'      => 'Especificações Técnicas',
        'aplicacoes'          => 'Aplicações',
        'relacionados'        => 'Produtos relacionados',
        'topo'                => 'Topo',
        'orcamento-info'      => 'Orçamento | Mais informações',
        'sucesso'             => 'Orçamento enviado com sucesso!'
    ],

    'busca' => [
        'titulo' => 'Resultado da Busca',
        'nenhum' => 'Nenhum resultado encontrado.'
    ]

];
