<!DOCTYPE html>
<html>
<head>
    <title>[TRABALHE CONOSCO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
@if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Área de Interesse:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $area_de_interesse }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Currículo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ asset('curriculos/'.$curriculo) }}" target="_blank">Download</a></span>
</body>
</html>
