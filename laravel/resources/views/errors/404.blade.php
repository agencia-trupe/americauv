@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <h1>{{ trans('frontend.404') }}</h1>
    </div>

@endsection
