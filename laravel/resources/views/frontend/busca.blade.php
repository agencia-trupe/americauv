@extends('frontend.common.template')

@section('content')

    <div class="main busca">
        <div class="center">
            <h1>{{ t('busca.titulo') }}</h1>
            <div class="resultados">
                @if(! count($resultados))
                    <div class="texto">
                        <p>{{ t('busca.nenhum') }}</p>
                    </div>
                @else
                    @foreach($resultados as $r)
                    <a href="{{ $r['link'] }}">
                        <h3>{{ $r['area'] }}</h3>
                        <p>
                            <span>{{ $r['titulo'] }}</span>
                            {{ $r['texto'] }}
                        </p>
                    </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@endsection
