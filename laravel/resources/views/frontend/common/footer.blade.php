    <footer>
        <div class="center colunas">
            <div class="col">
                <span class="titulo">{{ t('nav.empresa') }}</span>
                <a href="javascript:void(0)" class="pai">{{ t('nav.america-uv') }}</a>
                <a href="{{ route('empresa.institucional') }}">{{ t('nav.institucional') }}</a>
                <a href="{{ route('empresa.cd-logistico') }}">{{ t('nav.cd-logistico') }}</a>
                <a href="{{ route('empresa.galeria') }}">{{ t('nav.galeria') }}</a>
                <a href="{{ route('suporte') }}" class="pai">{{ t('nav.suporte-tecnico') }}</a>
                <a href="#" class="pai">{{ t('nav.parceiros') }}</a>
            </div>
            <div class="col">
                <span class="titulo">{{ t('nav.produtos') }}</span>
                <a href="javascript:void(0)" class="pai">{{ t('nav.tecnologias') }}</a>
                <a href="{{ route('tecnologias', 'impressoras-para-rigidos') }}">{{ t('nav.impressoras-para-rigidos') }}</a>
                <a href="{{ route('tecnologias', 'impressoras-para-flexiveis') }}">{{ t('nav.impressoras-para-flexiveis') }}</a>
                <a href="{{ route('tecnologias', 'impressoras-hibridas') }}">{{ t('nav.impressoras-hibridas') }}</a>
                <a href="{{ route('tecnologias', 'sistemas-de-corte') }}">{{ t('nav.sistemas-de-corte') }}</a>
                <a href="{{ route('tinta-digital-uv-led-hnd-ink') }}" class="pai">{{ t('nav.tinta-digital-uv-led-hnd-ink') }}</a>
                <a href="{{ route('materiais') }}" class="pai">{{ t('nav.materiais') }}</a>
            </div>
            <div class="col">
                <span class="titulo">{{ t('nav.contato') }}</span>
                <a href="{{ route('trabalhe') }}" class="pai">{{ t('nav.trabalhe-conosco') }}</a>
                <a href="{{ route('contato') }}" class="pai">{{ t('nav.contato') }}</a>
                <p class="telefones">
                    <a href="https://api.whatsapp.com/send?phone={{ str_replace([' ', '+', '-'], '', $contato->whatsapp) }}" class="whatsapp" target="_blank">{{ $contato->whatsapp }}</a>
                    <span class="break">{!! $contato->telefones !!}</span>
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            </div>
            <div class="newsletter">
                <p>{{ t('footer.novidades') }}:</p>
                <form action="{{ route('newsletter') }}" id="form-newsletter">
                    <input type="email" name="newsletter_email" placeholder="e-mail" required>
                    <button></button>
                </form>
                <div class="social-wrapper">
                    <img src="{{ asset('assets/img/layout/marca-americauv.png') }}" alt="">
                    <div class="social">
                        <p>{{ t('footer.siga-nos') }}:</p>
                        @foreach(['instagram', 'facebook', 'youtube', 'linkedin', 'twitter'] as $s)
                            @if($contato->{$s})
                            <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} - {{ t('footer.copyright') }}.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">{{ t('footer.sites') }}</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
