    <header>
        <div class="center">
            <div class="topo">
                <a href="#" class="busca-handle">busca</a>
                <div class="social">
                    <a href="https://api.whatsapp.com/send?phone={{ str_replace([' ', '+', '-'], '', $contato->whatsapp) }}" class="whatsapp" target="_blank">{{ $contato->whatsapp }}</a>
                    @foreach(['instagram', 'facebook', 'youtube', 'linkedin', 'twitter'] as $s)
                        @if($contato->{$s})
                        <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
                <div class="lang">
                    @foreach(['pt', 'en', 'es'] as $l)
                        @if(app()->getLocale() != $l)
                        <a href="{{ route('lang', $l) }}" class="{{ $l }}">{{ strtoupper($l) }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
            <a href="{{ route('home') }}" class="logo">
                {{ config('app.name') }}
            </a>
            <nav id="nav-desktop">
                <div class="dropdown-handle">
                    <a href="javascript:void(0)" @if(Tools::routeIs('empresa.*')) class="active" @endif>{{ t('nav.america-uv') }}</a>
                    <div class="dropdown">
                        <div class="center">
                            <a href="{{ route('empresa.institucional') }}" @if(Tools::routeIs('empresa.institucional')) class="active" @endif>
                                {{ t('nav.institucional') }}
                                <img src="{{ asset('assets/img/layout/icone-america-institucional.png') }}" alt="">
                            </a>
                            <a href="{{ route('empresa.cd-logistico') }}" @if(Tools::routeIs('empresa.cd-logistico')) class="active" @endif>
                                {{ t('nav.cd-logistico') }}
                                <img src="{{ asset('assets/img/layout/icone-america-cdlogistico.png') }}" alt="">
                            </a>
                            <a href="{{ route('empresa.galeria') }}" @if(Tools::routeIs('empresa.galeria*')) class="active" @endif>
                                {{ t('nav.galeria') }}
                                <img src="{{ asset('assets/img/layout/icone-america-galeria.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="dropdown-handle">
                    <a href="javascript:void(0)" @if(Tools::routeIs('tecnologias*')) class="active" @endif>{{ t('nav.tecnologias') }}</a>
                    <div class="dropdown">
                        <div class="center">
                            <a href="{{ route('tecnologias', 'impressoras-para-rigidos') }}" @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-para-rigidos') class="active" @endif>
                                {{ t('nav.impressoras-para-rigidos') }}
                                <img src="{{ asset('assets/img/layout/icone-impr-rigidos.png') }}" alt="">
                            </a>
                            <a href="{{ route('tecnologias', 'impressoras-para-flexiveis') }}" @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-para-flexiveis') class="active" @endif>
                                {{ t('nav.impressoras-para-flexiveis') }}
                                <img src="{{ asset('assets/img/layout/icone-impr-flexiveis.png') }}" alt="">
                            </a>
                            <a href="{{ route('tecnologias', 'impressoras-hibridas') }}" @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-hibridas') class="active" @endif>
                                {{ t('nav.impressoras-hibridas') }}
                                <img src="{{ asset('assets/img/layout/icone-impr-hibridas.png') }}" alt="">
                            </a>
                            <a href="{{ route('tecnologias', 'sistemas-de-corte') }}" @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'sistemas-de-corte') class="active" @endif>
                                {{ t('nav.sistemas-de-corte') }}
                                <img src="{{ asset('assets/img/layout/icone-impr-sistemacorte.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <a href="{{ route('tinta-digital-uv-led-hnd-ink') }}" @if(Tools::routeIs('tintas*')) class="active" @endif>{{ t('nav.tinta-digital-uv-led-hnd-ink') }}</a>
                <a href="{{ route('materiais') }}" @if(Tools::routeIs('materiais*')) class="active" @endif>{{ t('nav.materiais') }}</a>
                <a href="{{ route('suporte') }}" @if(Tools::routeIs('suporte')) class="active" @endif>{{ t('nav.suporte-tecnico') }}</a>
                <a href="{{ route('parceiros') }}" @if(Tools::routeIs('parceiros')) class="active" @endif>{{ t('nav.parceiros') }}</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>{{ t('nav.contato') }}</a>
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <div id="nav-mobile">
        <div class="center">
            <a href="javascript:void(0)" @if(Tools::routeIs('empresa.*')) class="active" @endif>{{ t('nav.america-uv') }}</a>
            <a href="{{ route('empresa.institucional') }}" class="sub @if(Tools::routeIs('empresa.institucional')) active @endif">{{ t('nav.institucional') }}</a>
            <a href="{{ route('empresa.cd-logistico') }}" class="sub @if(Tools::routeIs('empresa.cd-logistico')) active @endif">{{ t('nav.cd-logistico') }}</a>
            <a href="{{ route('empresa.galeria') }}" class="sub @if(Tools::routeIs('empresa.galeria*')) active @endif">{{ t('nav.galeria') }}</a>

            <a href="javascript:void(0)" @if(Tools::routeIs('tecnologias*')) class="active" @endif>{{ t('nav.tecnologias') }}</a>
            <a href="{{ route('tecnologias', 'impressoras-para-rigidos') }}" class="sub @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-para-rigidos') active @endif">
                {{ t('nav.impressoras-para-rigidos') }}
            </a>
            <a href="{{ route('tecnologias', 'impressoras-para-flexiveis') }}" class="sub @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-para-flexiveis') active @endif">
                {{ t('nav.impressoras-para-flexiveis') }}
            </a>
            <a href="{{ route('tecnologias', 'impressoras-hibridas') }}" class="sub @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'impressoras-hibridas') active @endif">
                {{ t('nav.impressoras-hibridas') }}
            </a>
            <a href="{{ route('tecnologias', 'sistemas-de-corte') }}" class="sub @if(Tools::routeIs('tecnologias*') && $tecnologiaSlug == 'sistemas-de-corte') active @endif">
                {{ t('nav.sistemas-de-corte') }}
            </a>

            <a href="{{ route('tinta-digital-uv-led-hnd-ink') }}" @if(Tools::routeIs('tintas*')) class="active" @endif>{{ t('nav.tinta-digital-uv-led-hnd-ink') }}</a>
            <a href="{{ route('materiais') }}" @if(Tools::routeIs('materiais*')) class="active" @endif>{{ t('nav.materiais') }}</a>
            <a href="{{ route('suporte') }}" @if(Tools::routeIs('suporte')) class="active" @endif>{{ t('nav.suporte-tecnico') }}</a>
            <a href="{{ route('parceiros') }}" @if(Tools::routeIs('parceiros')) class="active" @endif>{{ t('nav.parceiros') }}</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>{{ t('nav.contato') }}</a>
        </div>
    </div>

    <div class="modal-busca">
        <form action="{{ route('busca') }}" method="GET">
            <input type="text" name="termo" placeholder="{{ t('nav.buscar') }}" required>
            <button></button>
        </form>
    </div>
