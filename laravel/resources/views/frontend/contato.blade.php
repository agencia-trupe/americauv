@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h1>{{ t('nav.contato') }}</h1>
            <div class="left">
                <div class="texto">{!! tobj($contato, 'texto_contato') !!}</div>
                <form action="{{ route('contato.post') }}" method="POST" class="form-box">
                    {!! csrf_field() !!}

                    <h3>{{ t('contato.fale-conosco') }}</h3>

                    <input type="text" name="nome" value="{{ old('nome') }}" placeholder="{{ t('contato.nome') }}" required>
                    <input type="text" name="empresa" value="{{ old('empresa') }}" placeholder="{{ t('contato.empresa') }}">
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ t('contato.telefone') }}">
                    <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="{{ t('contato.enviar') }}">

                    @if(session('enviado'))
                    <div class="flash">{{ t('contato.sucesso') }}</div>
                    @endif
                    @if($errors->any())
                    <div class="flash">{{ t('contato.erro') }}</div>
                    @endif
                </form>
            </div>
            <div class="right">
                <p class="telefones">
                    <a href="https://api.whatsapp.com/send?phone={{ str_replace([' ', '+', '-'], '', $contato->whatsapp) }}" class="whatsapp" target="_blank">{{ $contato->whatsapp }} <span></span></a>
                    {!! $contato->telefones !!}
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
            </div>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
