@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="cd-logistico">
            <div class="center">
                <div class="left texto">
                    <h1>{{ t('nav.cd-logistico') }}</h1>
                    {!! tobj($cdLogistico, 'texto_1') !!}
                    <img src="{{ asset('assets/img/cd-logistico/'.$cdLogistico->imagem_1) }}" alt="">
                </div>
                <div class="right texto">
                    <img src="{{ asset('assets/img/cd-logistico/'.$cdLogistico->imagem_2) }}" alt="">
                    {!! tobj($cdLogistico, 'texto_2') !!}
                </div>
            </div>
        </div>
    </div>

@endsection
