@extends('frontend.common.template')

@section('content')

    <div class="main galeria">
        <div class="center">
            <h1>{{ t('nav.galeria') }}</h1>
            <div class="galeria-thumbs">
                @foreach($galerias as $g)
                <a href="{{ route('empresa.galeria.show', $g->slug) }}">
                    <img src="{{ asset('assets/img/galeria/'.$g->capa) }}" alt="">
                    <div>
                        <h3>{{ tobj($g, 'titulo') }}</h3>
                        <p>{{ tobj($g, 'subtitulo') }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            {!! $galerias->render() !!}
        </div>
    </div>

@endsection
