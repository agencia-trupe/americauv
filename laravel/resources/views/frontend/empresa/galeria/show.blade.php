@extends('frontend.common.template')

@section('content')

    <div class="main galeria">
        <div class="center">
            <h1>{{ t('nav.galeria') }}</h1>
            <div class="galeria-show">
                <h2>{{ tobj($galeria, 'titulo') }}</h2>
                <div class="right texto">
                    <h4>{{ tobj($galeria, 'subtitulo') }}</h4>
                    {!! tobj($galeria, 'texto') !!}
                </div>
                <div class="left">
                    <img src="{{ asset('assets/img/galeria/'.$galeria->capa) }}" alt="">
                    @foreach($galeria->imagens as $i)
                    <img src="{{ asset('assets/img/galeria/imagens/'.$i->imagem) }}" alt="">
                    @endforeach
                </div>
                @if($galeria->{'video_'.app()->getLocale()})
                <div class="clear-video">
                    <div class="video">
                        <div class="video-wrapper">
                            <iframe src="{{ tobj($galeria, 'video') }}" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                @endif
                <div class="clear-video">
                    <div class="voltar">
                        <a href="{{ route('empresa.galeria') }}">
                            &laquo; {{ t('empresa.galeria-voltar') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
