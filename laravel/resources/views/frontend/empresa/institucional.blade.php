@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="institucional">
            <div class="center">
                <div class="left texto">
                    <h1>{{ t('nav.institucional') }}</h1>
                    {!! tobj($institucional, 'texto') !!}
                </div>
                <div class="right">
                    <img src="{{ asset('assets/img/institucional/'.$institucional->imagem) }}" alt="">
                    <div class="box">
                        @foreach(['missao', 'visao', 'valores'] as $t)
                        <div class="row">
                            <div>
                                <h3>{{ t('empresa.'.$t) }}</h3>
                                <img src="{{ asset('assets/img/layout/icone-'.$t.'.png') }}" alt="">
                            </div>
                            <div class="texto">
                                {!! tobj($institucional, $t) !!}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="clear-video">
                    <div class="video">
                        <div class="video-wrapper">
                            <iframe src="{{ tobj($institucional, 'video') }}" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
