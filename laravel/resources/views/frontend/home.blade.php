@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }}">
                <div class="center">
                    @if($banner->{'texto_'.app()->getLocale()})
                    <span class="{{ $banner->botao_cor }}">{!! tobj($banner, 'texto') !!}</span>
                    @endif
                    @if($banner->link)
                    <a href="{{ Tools::parseLink($banner->link) }}" class="{{ $banner->botao_cor }}">
                        {!! tobj($banner, 'botao') !!}
                    </a>
                    @endif
                </div>
            </div>
            @endforeach
        </div>

        <div class="center">
            <div class="chamadas">
                @foreach($chamadas as $c)
                <a href="{{ Tools::parseLink($c->link) }}">
                    <img src="{{ asset('assets/img/chamadas/'.$c->imagem) }}" alt="">
                    <span>{{ tobj($c, 'titulo') }}</span>
                </a>
                @endforeach
            </div>

            <div class="home-texto">
                <div class="titulo">{{ tobj($home, 'titulo') }}</div>
                <div class="texto">
                    <div class="imagens">
                        @foreach(range(1, 4) as $i)
                        <img src="{{ asset('assets/img/home/'.$home->{'imagem_'.$i}) }}" alt="">
                        @endforeach
                    </div>
                    {!! tobj($home, 'texto') !!}
                </div>
            </div>
        </div>

        <div class="novidades">
            <div class="center">
                <h3>{{ t('nav.novidades') }}</h3>
                <div class="novidades-thumbs">
                    @foreach($novidades as $n)
                    <a href="{{ route('empresa.galeria.show', $n->slug) }}">
                        <img src="{{ asset('assets/img/galeria/'.$n->capa) }}" alt="">
                        <h4>{{ tobj($n, 'titulo') }}</h4>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="depoimentos">
            <div class="center">
                <div class="depoimentos-cycle">
                    @foreach($depoimentos as $d)
                    <div class="depoimento">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/depoimentos/'.$d->imagem) }}" alt="">
                        </div>
                        <div class="depoimento-texto">
                            <p>{!! tobj($d, 'texto') !!}</p>
                            <p class="info">
                                <strong>{{ $d->nome }}</strong>
                                {{ $d->empresa }}
                            </p>
                        </div>
                    </div>
                    @endforeach
                    <span class="next"></span>
                </div>
            </div>
        </div>
    </div>

@endsection
