@extends('frontend.common.template')

@section('content')

    <div class="main parceiros">
        <div class="center">
            <div class="left">
                <h1>{{ t('nav.parceiros') }}</h1>
                <div class="texto">{!! tobj($parceiros, 'texto') !!}</div>
                <form action="{{ route('parceiros.post') }}" method="POST" class="form-box" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <h3>{{ t('contato.quero-parceiro') }}</h3>

                    <input type="text" name="nome" value="{{ old('nome') }}" placeholder="{{ t('contato.nome') }}" required>
                    <input type="text" name="empresa" value="{{ old('empresa') }}" placeholder="{{ t('contato.empresa') }}" required>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ t('contato.telefone') }}">
                    <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                    <label class="input-arquivo">
                        <span>{{ t('contato.apresentacao') }}</span>
                        <input type="file" name="apresentacao" required>
                    </label>
                    <input type="submit" value="{{ t('contato.enviar') }}">

                    @if(session('enviado'))
                    <div class="flash">{{ t('contato.sucesso') }}</div>
                    @endif
                    @if($errors->any())
                    <div class="flash">
                        {{ t('contato.erro') }}
                        @foreach($errors->get('apresentacao') as $m)
                        <br>{{ $m }}
                        @endforeach
                    </div>
                    @endif
                </form>
            </div>
            <div class="right">
                <img src="{{ asset('assets/img/parceiros/'.$parceiros->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
