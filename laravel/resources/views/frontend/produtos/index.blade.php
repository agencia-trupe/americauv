@extends('frontend.common.template')

@section('content')

    <div class="main produtos-index">
        <div class="center">
            <h1>{{ t('nav.'.$categoria->slug) }}</h1>
            <div class="left texto">
                {!! tobj($categoria, 'texto') !!}
            </div>
            <div class="right">
                <div class="produtos-thumbs">
                    @foreach($categoria->produtos as $p)
                    <a href="{{ $p->showLink }}">
                        <img src="{{ asset('assets/img/produtos/miniatura/'.$p->miniatura) }}" alt="">
                        <h3>{{ tobj($p, 'titulo') }}</h3>
                        <p>{{ tobj($p, 'subtitulo') }}</p>
                        <span>{{ t('produtos.saiba-mais') }}</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
