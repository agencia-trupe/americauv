    <div class="lightbox box-orcamento">
        <h3>{{ t('produtos.orcamento-info') }}</h3>
        <h2>{{ tobj($produto, 'titulo') }}</h2>

        <form action="" id="form-orcamento-lightbox" class="form-box" method="POST">
            <input type="hidden" name="produto" id="l_produto" value="{{ $produto->categoria->titulo }} | {{ $produto->titulo_pt }}">
            <input type="text" name="nome" id="l_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
            <input type="email" name="email" id="l_email" placeholder="e-mail" required>
            <input type="text" name="telefone" id="l_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
            <textarea name="mensagem" id="l_mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
            <div id="form-orcamento-lightbox-response" class="flash"></div>
            <button>{!! t('produtos.solicitar-orcamento') !!}</button>
        </form>
    </div>
