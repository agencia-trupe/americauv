@extends('frontend.common.template')

@section('content')

    <div class="produtos-show">
        <div class="capa capa-mobile preto">
            <div class="center">
                <h2>{{ t('nav.'.$produto->categoria->slug) }}</h2>
                <h1>{{ tobj($produto, 'titulo') }}</h1>
            </div>
        </div>
        <div class="capa {{ $produto->cor_texto_capa }}" style="background-image:url({{ asset('assets/img/produtos/'.$produto->capa) }})">
            <div class="center">
                <h2>{{ t('nav.'.$produto->categoria->slug) }}</h2>
                <h1>{{ tobj($produto, 'titulo') }}</h1>
            </div>
        </div>
        <div class="main">
            <div class="center">
                <div class="texto">
                    {!! tobj($produto, 'texto') !!}
                    <div class="botoes">
                        @if($produto->{'catalogo_'.app()->getLocale()})
                        <a href="{{ url('assets/catalogos/'.tobj($produto, 'catalogo')) }}" class="catalogo" target="_blank">{!! t('produtos.catalogo') !!}</a>
                        @endif
                        <a href="{{ route('orcamento', $produto->slug) }}" class="orcamento lightbox-content">{!! t('produtos.solicitar-orcamento') !!}</a>
                    </div>
                </div>

                @if(count($produto->imagens) || count($produto->especificacoes) || $produto->{'video_'.app()->getLocale()})
                <div class="informacoes">
                    @if(count($produto->imagens))
                    <div class="imagens">
                        @foreach($produto->imagens as $i)
                        <img src="{{ asset('assets/img/produtos/imagens/'.$i->imagem) }}" alt="">
                        @endforeach
                    </div>
                    @endif

                    @if(count($produto->especificacoes))
                    <div class="especificacoes">
                        <h4>{{ t('produtos.especificacoes') }}</h4>
                        <h3>{{ tobj($produto, 'titulo') }}</h3>
                        <div class="tabela">
                            @foreach($produto->especificacoes as $e)
                            <div class="row">
                                <span>{{ tobj($e, 'especificacao') }}</span>
                                <span>{{ tobj($e, 'valor') }}</span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    @if($produto->{'video_'.app()->getLocale()})
                    <div class="clear-video">
                        <div class="video">
                            <div class="video-wrapper">
                                <iframe src="{{ tobj($produto, 'video') }}" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endif

                @if(count($produto->aplicacoes))
                <div class="aplicacoes">
                    <h3>{{ t('produtos.aplicacoes') }}</h3>
                    <div>
                        @foreach($produto->aplicacoes as $a)
                        <img src="{{ asset('assets/img/produtos/aplicacoes/'.$a->imagem) }}" alt="">
                        @endforeach
                    </div>
                </div>
                @endif
            </div>

            @if(count($produto->relacionados()))
            <div class="relacionados">
                <div class="center">
                    <h3>{{ t('produtos.relacionados') }}</h3>
                    <div class="relacionados-thumbs">
                        @foreach($produto->relacionados() as $p)
                        <a href="{{ $p->showLink }}">
                            <img src="{{ asset('assets/img/produtos/miniatura/'.$p->miniatura) }}" alt="">
                            <p>{{ tobj($p, 'titulo') }}</p>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif

            <a href="#" class="btn-topo">{{ t('produtos.topo') }} <span>&uarr;</span></a>
        </div>
    </div>

@endsection
