@extends('frontend.common.template')

@section('content')

    <div class="main suporte">
        <div class="center">
            <h1>{{ t('nav.suporte-tecnico') }}</h1>
            <div class="left">
                <div class="texto">{!! tobj($suporte, 'texto') !!}</div>
            </div>
            <div class="right">
                <img src="{{ asset('assets/img/suporte-tecnico/'.$suporte->imagem) }}" alt="">
                <form action="{{ route('suporte.post') }}" method="POST" class="form-box">
                    {!! csrf_field() !!}

                    <h3>{{ t('nav.suporte') }}</h3>

                    <input type="text" name="nome" value="{{ old('nome') }}" placeholder="{{ t('contato.nome') }}" required>
                    <input type="text" name="empresa" value="{{ old('empresa') }}" placeholder="{{ t('contato.empresa') }}">
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ t('contato.telefone') }}">
                    <select name="assunto" required>
                        <option value="">{{ t('contato.assunto') }}</option>
                        @foreach($assuntos as $a)
                        <option value="{{ $a->nome_pt }}" @if(old('assunto') == $a->nome_pt) selected @endif>{{ tobj($a, 'nome') }}</option>
                        @endforeach
                    </select>
                    <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="{{ t('contato.enviar') }}">

                    @if(session('enviado'))
                    <div class="flash">{{ t('contato.sucesso') }}</div>
                    @endif
                    @if($errors->any())
                    <div class="flash">{{ t('contato.erro') }}</div>
                    @endif
                </form>
                <div class="faq">
                    <h2>FAQ</h2>
                    @foreach($faq as $p)
                    <a href="#">
                        {{ tobj($p, 'pergunta') }}
                        <span></span>
                    </a>
                    <div class="texto" style="display:none">{!! tobj($p, 'resposta') !!}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
