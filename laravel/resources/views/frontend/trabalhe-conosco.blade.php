@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h1>{{ t('nav.trabalhe-conosco') }}</h1>
            <div class="left">
                <div class="texto">{!! tobj($contato, 'texto_trabalhe_conosco') !!}</div>
                <form action="{{ route('trabalhe.post') }}" method="POST" class="form-box" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <h3>{{ t('nav.trabalhe-conosco') }}</h3>

                    <input type="text" name="nome" value="{{ old('nome') }}" placeholder="{{ t('contato.nome') }}" required>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ t('contato.telefone') }}">
                    <select name="area_de_interesse" required>
                        <option value="">{{ t('contato.area-de-interesse') }}</option>
                        @foreach($areas as $a)
                        <option value="{{ $a->nome_pt }}" @if(old('area_de_interesse') == $a->nome_pt) selected @endif>{{ tobj($a, 'nome') }}</option>
                        @endforeach
                    </select>
                    <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                    <label class="input-arquivo">
                        <span>{{ t('contato.curriculo') }}</span>
                        <input type="file" name="curriculo" required>
                    </label>
                    <input type="submit" value="{{ t('contato.enviar') }}">

                    @if(session('enviado'))
                    <div class="flash">{{ t('contato.sucesso') }}</div>
                    @endif
                    @if($errors->any())
                    <div class="flash">
                        {{ t('contato.erro') }}
                        @foreach($errors->get('curriculo') as $m)
                        <br>{{ $m }}
                        @endforeach
                    </div>
                    @endif
                </form>
            </div>
            <div class="right">
                <p class="telefones">
                    <a href="https://api.whatsapp.com/send?phone={{ str_replace([' ', '+', '-'], '', $contato->whatsapp) }}" class="whatsapp" target="_blank">{{ $contato->whatsapp }} <span></span></a>
                    {!! $contato->telefones !!}
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
            </div>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
