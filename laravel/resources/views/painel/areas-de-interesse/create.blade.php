@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Interesse /</small> Adicionar Área de Interesse</h2>
    </legend>

    {!! Form::open(['route' => 'painel.areas-de-interesse.store', 'files' => true]) !!}

        @include('painel.areas-de-interesse.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
