@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Interesse /</small> Editar Área de Interesse</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.areas-de-interesse.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.areas-de-interesse.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
