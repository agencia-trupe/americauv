@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome_pt', 'Nome [PT]') !!}
    {!! Form::text('nome_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_en', 'Nome [EN]') !!}
    {!! Form::text('nome_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_es', 'Nome [ES]') !!}
    {!! Form::text('nome_es', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.areas-de-interesse.index') }}" class="btn btn-default btn-voltar">Voltar</a>
