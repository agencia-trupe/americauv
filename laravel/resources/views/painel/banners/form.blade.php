@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_es', 'Texto [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well">
    <div class="form-group">
        {!! Form::label('botao_pt', 'Texto do Botão [PT]') !!}
        {!! Form::text('botao_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('botao_en', 'Texto do Botão [EN]') !!}
        {!! Form::text('botao_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('botao_es', 'Texto do Botão [ES]') !!}
        {!! Form::text('botao_es', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group" style="margin-bottom:0">
        {!! Form::label('botao_cor', 'Cor do Botão/Texto') !!}
        {!! Form::select('botao_cor', ['preto' => 'Preto', 'branco' => 'Branco'], null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
