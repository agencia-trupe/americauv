@extends('painel.common.template')

@section('content')

    <legend>
        <h2>CD Logístico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cd-logistico.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cd-logistico.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
