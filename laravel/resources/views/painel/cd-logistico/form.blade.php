@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_1_pt', 'Texto 1 [PT]') !!}
    {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_1_en', 'Texto 1 [EN]') !!}
    {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_1_es', 'Texto 1 [ES]') !!}
    {!! Form::textarea('texto_1_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_2_pt', 'Texto 2 [PT]') !!}
    {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2_en', 'Texto 2 [EN]') !!}
    {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2_es', 'Texto 2 [ES]') !!}
    {!! Form::textarea('texto_2_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/cd-logistico/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/cd-logistico/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
