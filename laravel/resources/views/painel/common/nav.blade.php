<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs(['painel.home*', 'painel.banners*', 'painel.chamadas*', 'painel.depoimentos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.chamadas*')) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
            <li @if(Tools::routeIs('painel.depoimentos*')) class="active" @endif>
                <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.institucional*', 'painel.cd-logistico*', 'painel.galeria*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            America UV
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.institucional*')) class="active" @endif>
                <a href="{{ route('painel.institucional.index') }}">Institucional</a>
            </li>
            <li @if(Tools::routeIs('painel.cd-logistico*')) class="active" @endif>
                <a href="{{ route('painel.cd-logistico.index') }}">CD Logístico</a>
            </li>
            <li @if(Tools::routeIs('painel.galeria*')) class="active" @endif>
                <a href="{{ route('painel.galeria.index') }}">Galeria</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
	</li>
    <li class="dropdown @if(Tools::routeIs(['painel.suporte-tecnico.index', 'painel.assuntos*', 'painel.faq*', 'painel.suporte-tecnico.recebidos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Suporte Técnico
            @if($suporteNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $suporteNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.suporte-tecnico.index')) class="active" @endif>
                <a href="{{ route('painel.suporte-tecnico.index') }}">Suporte Técnico</a>
            </li>
            <li @if(Tools::routeIs('painel.assuntos*')) class="active" @endif>
                <a href="{{ route('painel.assuntos.index') }}">Assuntos</a>
            </li>
            <li @if(Tools::routeIs('painel.faq*')) class="active" @endif>
                <a href="{{ route('painel.faq.index') }}">FAQ</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.suporte-tecnico.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.suporte-tecnico.recebidos.index') }}">
                    Solicitações Recebidas
                    @if($suporteNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $suporteNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.parceiros.index', 'painel.parceiros.recebidos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Parceiros
            @if($parceirosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $parceirosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.parceiros.index')) class="active" @endif>
                <a href="{{ route('painel.parceiros.index') }}">Parceiros</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.parceiros.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.parceiros.recebidos.index') }}">
                    Solicitações Recebidas
                    @if($parceirosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $parceirosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.contato*', 'painel.areas-de-interesse*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos + $curriculosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos + $curriculosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.contato.trabalhe-conosco*')) class="active" @endif>
                <a href="{{ route('painel.contato.trabalhe-conosco.index') }}">
                    Trabalhe Conosco
                    @if($curriculosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $curriculosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.areas-de-interesse*')) class="active" @endif>
                <a href="{{ route('painel.areas-de-interesse.index') }}">Áreas de Interesse <small>(Trabalhe Conosco)</small></a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.orcamentos*')) class="active" @endif>
        <a href="{{ route('painel.orcamentos.index') }}">
            Orçamentos
            @if($orcamentosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
