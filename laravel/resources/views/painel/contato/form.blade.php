@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('whatsapp', 'WhatsApp') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefones', 'Telefones') !!}
    {!! Form::textarea('telefones', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('instagram', 'Instagram') !!}
            {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('facebook', 'Facebook') !!}
            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('youtube', 'YouTube') !!}
            {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('linkedin', 'LinkedIn') !!}
            {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('twitter', 'Twitter') !!}
            {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_contato_pt', 'Texto Contato [PT]') !!}
            {!! Form::textarea('texto_contato_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_contato_en', 'Texto Contato [EN]') !!}
            {!! Form::textarea('texto_contato_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_contato_es', 'Texto Contato [ES]') !!}
            {!! Form::textarea('texto_contato_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_trabalhe_conosco_pt', 'Texto Trabalhe Conosco [PT]') !!}
            {!! Form::textarea('texto_trabalhe_conosco_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_trabalhe_conosco_en', 'Texto Trabalhe Conosco [EN]') !!}
            {!! Form::textarea('texto_trabalhe_conosco_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_trabalhe_conosco_es', 'Texto Trabalhe Conosco [ES]') !!}
            {!! Form::textarea('texto_trabalhe_conosco_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($contato->imagem)
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
