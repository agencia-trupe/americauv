@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos /</small> Editar Depoimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.depoimentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
