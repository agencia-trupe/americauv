@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('pergunta_pt', 'Pergunta [PT]') !!}
    {!! Form::text('pergunta_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pergunta_en', 'Pergunta [EN]') !!}
    {!! Form::text('pergunta_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pergunta_es', 'Pergunta [ES]') !!}
    {!! Form::text('pergunta_es', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('resposta_pt', 'Resposta [PT]') !!}
    {!! Form::textarea('resposta_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta_en', 'Resposta [EN]') !!}
    {!! Form::textarea('resposta_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta_es', 'Resposta [ES]') !!}
    {!! Form::textarea('resposta_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.faq.index') }}" class="btn btn-default btn-voltar">Voltar</a>
