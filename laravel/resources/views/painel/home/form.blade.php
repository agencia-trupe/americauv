@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título [PT]') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [EN]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ES]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="row">
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/home/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/home/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            @if($registro->imagem_3)
            <img src="{{ url('assets/img/home/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            @if($registro->imagem_4)
            <img src="{{ url('assets/img/home/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
