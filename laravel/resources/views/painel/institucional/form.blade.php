@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_pt', 'Missão [PT]') !!}
            {!! Form::textarea('missao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('missao_en', 'Missão [EN]') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('missao_es', 'Missão [ES]') !!}
            {!! Form::textarea('missao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao_pt', 'Visão [PT]') !!}
            {!! Form::textarea('visao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('visao_en', 'Visão [EN]') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('visao_es', 'Visão [ES]') !!}
            {!! Form::textarea('visao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores_pt', 'Valores [PT]') !!}
            {!! Form::textarea('valores_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valores_en', 'Valores [EN]') !!}
            {!! Form::textarea('valores_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valores_es', 'Valores [ES]') !!}
            {!! Form::textarea('valores_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('video_pt', 'Vídeo [PT]') !!}
    {!! Form::text('video_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_en', 'Vídeo [EN]') !!}
    {!! Form::text('video_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_es', 'Vídeo [ES]') !!}
    {!! Form::text('video_es', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
