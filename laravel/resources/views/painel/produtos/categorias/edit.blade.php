@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.produtos.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.produtos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
