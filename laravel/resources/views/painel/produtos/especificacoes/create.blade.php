@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} / Especificações /</small> Adicionar Especificação</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.especificacoes.store', $produto], 'files' => true]) !!}

        @include('painel.produtos.especificacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
