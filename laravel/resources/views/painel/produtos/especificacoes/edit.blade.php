@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} / Especificações /</small> Editar Especificação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.especificacoes.update', $produto, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.especificacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
