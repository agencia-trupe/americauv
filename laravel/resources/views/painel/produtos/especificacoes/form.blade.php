@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('especificacao_pt', 'Especificação [PT]') !!}
            {!! Form::text('especificacao_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('especificacao_en', 'Especificação [EN]') !!}
            {!! Form::text('especificacao_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('especificacao_es', 'Especificação [ES]') !!}
            {!! Form::text('especificacao_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valor_pt', 'Valor [PT]') !!}
            {!! Form::text('valor_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valor_en', 'Valor [EN]') !!}
            {!! Form::text('valor_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valor_es', 'Valor [ES]') !!}
            {!! Form::text('valor_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.especificacoes.index', $produto) }}" class="btn btn-default btn-voltar">Voltar</a>
