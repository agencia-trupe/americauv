@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título [PT]') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [EN]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ES]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_pt', 'Subtítulo [PT]') !!}
    {!! Form::text('subtitulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo_en', 'Subtítulo [EN]') !!}
    {!! Form::text('subtitulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo_es', 'Subtítulo [ES]') !!}
    {!! Form::text('subtitulo_es', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('miniatura', 'Miniatura (285x160px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/miniatura/'.$registro->miniatura) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('miniatura', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (1920x620px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group" style="margin-bottom:0">
    {!! Form::label('cor_texto_capa', 'Cor do Texto da Capa') !!}
    {!! Form::select('cor_texto_capa', ['preto' => 'Preto', 'branco' => 'Branco'], null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('catalogo_pt', 'Catálogo [PT]') !!}
            @if($submitText == 'Alterar' && $registro->catalogo_pt)
            <p>
                <a href="{{ url('assets/catalogos/'.$registro->catalogo_pt) }}" target="_blank" style="display:block;margin-bottom:3px">{{ $registro->catalogo_pt }}</a>
                <a href="{{ route('painel.produtos.deleteCatalogo', [$registro->id, 'pt']) }}" class="btn btn-sm btn-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-trash" style="margin-right:4px"></span> Excluir catálogo</a>
            </p>
            @endif
            {!! Form::file('catalogo_pt', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('catalogo_en', 'Catálogo [EN]') !!}
            @if($submitText == 'Alterar' && $registro->catalogo_en)
            <p>
                <a href="{{ url('assets/catalogos/'.$registro->catalogo_en) }}" target="_blank" style="display:block;margin-bottom:3px">{{ $registro->catalogo_en }}</a>
                <a href="{{ route('painel.produtos.deleteCatalogo', [$registro->id, 'en']) }}" class="btn btn-sm btn-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-trash" style="margin-right:4px"></span> Excluir catálogo</a>
            </p>
            @endif
            {!! Form::file('catalogo_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('catalogo_pt', 'Catálogo [ES]') !!}
            @if($submitText == 'Alterar' && $registro->catalogo_es)
            <p>
                <a href="{{ url('assets/catalogos/'.$registro->catalogo_es) }}" target="_blank" style="display:block;margin-bottom:3px">{{ $registro->catalogo_es }}</a>
                <a href="{{ route('painel.produtos.deleteCatalogo', [$registro->id, 'es']) }}" class="btn btn-sm btn-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-trash" style="margin-right:4px"></span> Excluir catálogo</a>
            </p>
            @endif
            {!! Form::file('catalogo_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('video_pt', 'Vídeo [PT]') !!}
            {!! Form::text('video_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('video_en', 'Vídeo [EN]') !!}
            {!! Form::text('video_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('video_es', 'Vídeo [ES]') !!}
            {!! Form::text('video_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
