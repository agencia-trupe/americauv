@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Suporte Técnico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.suporte-tecnico.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.suporte-tecnico.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
